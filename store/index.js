export const state = () =>({
    _estado:1,
    _datos:[]
});

export  const  getters = {
  getEstado(state){
    return state._estado;
  },
}

export const mutations = {
  mi_estado(state,payload){
    state._estado = 1;
    if(payload){
      state._estado = 2;
    }else{
      state._estado = 1;
    }

  },
  muestraDetalle(state,payload){
    if(payload && state._estado == 2){
      state._estado = 3;
    }
  }
}

export const actions = {
  cambiarEstado({commit},payload){
    commit('mi_estado',payload);
  },
  detalle({commit},payload){
    commit('muestraDetalle',payload);
  }
}
