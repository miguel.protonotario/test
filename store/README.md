# Fullstack Test

**Requerimientos previos de la implementación**
 * Instalacion de Node
 * Instalacion de Yarn
 * Instalacion de Git
  


Para la implementacion del proyecto sigue los siguientes pasos:
* Clona el proyecto
 ```
 $ git clone https://gitlab.com/ajcarrillo/paenms
 $ git checkout -b devTuNombre
 $ git pull origin master

 ```

Seguidamente, de haber  clonado el proyecto, sigue el siguiente procedimiento:

* Agrega Nuxt con el comando
  ```
  yarn add nuxt
  ```
* Elimina el archivo yarn.lock (en caso que existiera)
* Elimina la carpeta node_modules (en caso que existiera)
* Ejecuta el comando yarn (en caso que existiera)
* Después de que termine la instalación y hayas ejecutado los tests, considera actualizar también las dependencias. Puedes usar el comando :
```  
yarn outdated
```
* Por ultimo puedes compilar con

```
npm run dev
```
