# Fullstack Test

## Dependencias

* `node`
* `yarn`
* `Git`

Para instalar `yarn` ejecuta en la terminal:

```
$ npm install --global yarn
```

## Proceso de instalacion

Para la instalacion de este proyecto sigue los siguientes pasos:

 ```
 
 $ git clone https://gitlab.com/miguel.protonotario/test.git
 $ git checkout -b devTuNombre
 
 // Baja los ultimos cambios del proyecto
 $ git pull origin devMiguel
 
 ```

 * Clona el proyecto
 * Crea tu rama de trabajo
 * Baja los últimos cambios en la rama `devMiguel`
 * Compilar proyecto



## Compilacion del proyecto
```
# instalacion de dependencias
$ yarn install

# Cargar servidor desde localhost:3000
$ yarn dev

```
